# repohomeiosstuff

this is a linking document for repositories that are stored in my 
bitbucket account. 

This document links to iOS development.

**here are the repos**

I have written some code related iOS. i use this while i am working on commercial projects. I also use this code during training sessions.

---

Beginner repos. 

These are repos I made while learning iOS for the first time. I still use them when I want to do some refreshing.

1. [https://bitbucket.org/thechalakas/worldtrotter]

2. [https://bitbucket.org/thechalakas/worldtrotter2]

20. [https://bitbucket.org/thechalakas/homepwner]

21. [https://bitbucket.org/thechalakas/myplayground5]

22. [https://bitbucket.org/thechalakas/playgroundcode]

23. [https://bitbucket.org/thechalakas/quiz]

24. [https://bitbucket.org/thechalakas/quiz2]

25. [https://bitbucket.org/thechalakas/quiz2]

26. [https://bitbucket.org/thechalakas/webapipractice1]

Edureka modules.

These are repos that I used when I taught some students over on edureka platform. These repos are a lot more structured. I suppose they must be because big brands have bigger brains than mine working on thier gameplan. 


3. [https://bitbucket.org/thechalakas/edurekamodule10a]

4. [https://bitbucket.org/thechalakas/edurekamodule10b]

5. [https://bitbucket.org/thechalakas/edurekamodule3a]

6. [https://bitbucket.org/thechalakas/edurekamodule5a]

7. [https://bitbucket.org/thechalakas/edurekamodule6a]

8. [https://bitbucket.org/thechalakas/edurekamodule6b]

9. [https://bitbucket.org/thechalakas/edurekamodule6c]

10. [https://bitbucket.org/thechalakas/edurekamodule6d]

11. [https://bitbucket.org/thechalakas/edurekamodule7a]

12. [https://bitbucket.org/thechalakas/edurekamodule7b]

13. [https://bitbucket.org/thechalakas/edurekamodule7c]

14. [https://bitbucket.org/thechalakas/edurekamodule7d]

15. [https://bitbucket.org/thechalakas/edurekamodule9a]

16. [https://bitbucket.org/thechalakas/edurekamodule9b]

17. [https://bitbucket.org/thechalakas/edurekamodule9c]


you know, for now, just dont use these bonus repos. I have no idea what is in them. 


18. [https://bitbucket.org/thechalakas/edurekamodulebonusa]

19. [https://bitbucket.org/thechalakas/edurekamodulebonusb]



---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 